package edu.sjsu.android.exercise4matthewzuberbuhler;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    private String data;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) data = getArguments().getString("myText");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        // Set the result TextView to the received data
        ((TextView) view.findViewById(R.id.result)).setText(data);
        // Initialize the NavController
        NavController controller = NavHostFragment.findNavController(this);
        // When "Get Text" button is clicked, navigate to TextFragment
        view.findViewById(R.id.get).setOnClickListener(v ->
                controller.navigate(R.id.action_mainFragment_to_textFragment));
        // When "Web Browser" button is clicked, call onClick
        view.findViewById(R.id.implicit).setOnClickListener(this::onClick);
        return view;
    }

    public void onClick(View view) {
        // Let the data to be amazon's homepage
        Uri website = Uri.parse("http://www.amazon.com");
        // Create an implicit Intent with a specific action (view) and the data
        Intent implicit = new Intent(Intent.ACTION_VIEW, website);
        // Create a chooser for the implicit Intent
        // so the user can choose the app to perform the action
        Intent choose = Intent.createChooser(implicit, "Load Amazon.com with:");
        // Use resultLauncher defined in MainActivity to start the Intent
        // so it can get result back
        MainActivity.resultLauncher.launch(choose);
    }
}