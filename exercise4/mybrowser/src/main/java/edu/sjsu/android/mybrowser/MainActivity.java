package edu.sjsu.android.mybrowser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String url = getIntent().getDataString();
        ((TextView) findViewById(R.id.website)).setText(url);

        findViewById(R.id.back).setOnClickListener(view -> {
            // Set the activity's result to RESULT_OK
                setResult(RESULT_OK);
            // Finish and close the current activity
                finish();
        });
    }
}