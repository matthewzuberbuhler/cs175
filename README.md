<h1><u>*** Project 4 ***</u></h1>

** Description: **

- An app that displays a Google map and allow users to switch between different views.

** Video Walkthrough **

<img src='/project4/project4_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/>

- - -

<h1><u>*** Exercise 6 ***</u></h1>

** Description: **

- This exercise provides an how to use a Google Map Activity.

** Video Walkthrough **

### Map and Location

[<img src='/exercise6/exercise6_map&location_Zuberbuhler_Matthew.gif' width="250"/>](exercise6_map&location_Zuberbuhler_Matthew.gif)

- - -

<h1><u>*** Exercise 5 ***</u></h1>

** Description: **

- This exercise provides an example for inserting and retrieving data from a database of another app through content provider.

** Video Walkthrough **

### Content Provider

[<img src='/exercise5/exercise5_ContentProvider_Zuberbuhler_Matthew.gif' width="250"/>](exercise5_ContentProvider_Zuberbuhler_Matthew.gif)

- - -

<h1><u>*** Exercise 4 ***</u></h1>

** Description: **

- An example of communicating between fragments of the same app and between different apps to display and manage a list .

** Video Walkthrough **

### Navigation & Intent App (Loading will take a few seconds)

[<img src='/exercise4/exercise4_NavigationAndIntent_Matthew.gif' width="250"/>](exercise4_NavigationAndIntent_Matthew.gif)

- - -

<h1><u>*** Exercise 3 ***</u></h1>

** Description: **

- An example of using the Recycle View to display and manage a list .

** Video Walkthrough **

### Recycle Viewer App

[<img src='/exercise3/exercise3_recyclerView_Matthew.gif' width="250"/>](exercise3_recyclerView_Matthew.gif)

- - -

<h1><u>*** Project 2 ***</u></h1>

** Description: **

- An interactive game where a ball falls according to the gravitation direction the phone currrently has.

** Video Walkthrough **

<img src='/project2/project2_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/>

- - -

<h1><u>*** Exercise 2 ***</u></h1>

** Description: **

- An example of using the mobile device sensors such as proximity sensors, lux sensors, and accelerometers. This exercise includes two modules: A Weather App and an Accelerometer Shake Test App.

** Video Walkthrough **

### Shaker App
<img src='/exercise2/exercise2_shaker_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/>

### Weather App
<img src='/exercise2/exercise2_weather_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/> 

- - -

<h1><u>*** Project 1 ***</u></h1>

** Description: **

- A mortgage calculator app that allows one to enter their principle amount, adjust their interest rate, and include taxes and insurance to view their monthly mortgage calculation.

** Video Walkthrough **

<img src='/project1/project1_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/>

- - -

<h1><u>*** Exercise 1 ***</u></h1>

** Description: **

- A temperature converter app that converts celcius to fahrenheit and vice versa.

** Video Walkthrough **

<img src='/exercise1/exercise1_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/>

- - -

<h1><u>*** Exercise 0 ***</u></h1>

** Description: **

- A simple app where the user can press a button and change the text on the view.

** Video Walkthrough **

<img src='/exercise0/exercise0_Zuberbuhler_Matthew.gif' title='Video Walkthrough' alt='Video Walkthrough' width="100"/>
