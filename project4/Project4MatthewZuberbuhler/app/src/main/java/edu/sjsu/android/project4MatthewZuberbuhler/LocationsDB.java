package edu.sjsu.android.project4MatthewZuberbuhler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * A class for a SQLite database of locations.
 */
class LocationsDB extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "locationsDatabase";
    private static final String TABLE_NAME = "locations";
    private static final int VERSION = 1;
    protected static final String Id = "_id";
    protected static final String latitude = "latitude";
    protected static final String longitude = "longitude";
    protected static final String zoomLevel = "zoomLevel";

    public LocationsDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_NAME +
                                                " (" + Id + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                                + latitude + " DOUBLE NOT NULL, "
                                                + longitude + " DOUBLE NOT NULL, "
                                                + zoomLevel + " FLOAT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /**
     * A method that inserts a new location to the table.
     *
     * @param contentValues location detail
     * @return the id
     */
    public long insert(ContentValues contentValues) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
    }

    /**
     * A method that deletes all locations from the table.
     *
     * @return number of locations deleted
     */
    public int deleteAll() {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.delete(TABLE_NAME, null, null);
    }

    /**
     * A method that returns all the locations from the table.
     *
     * @return Cursor
     */
    public Cursor getAllLocations() {
        // Remember to delete the throw statement after you done
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        return sqLiteDatabase.query(TABLE_NAME,
                                     new String[]{Id, latitude, longitude, zoomLevel},
                                    null, null, null, null,null);
    }
}