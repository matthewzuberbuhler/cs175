package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    boolean buttonSwitchBoolean = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onBtnClick(View view) {
        TextView helloWorldLabel = findViewById(R.id.helloWorldLabel);

        if(buttonSwitchBoolean == false)
        {
            helloWorldLabel.setText("Hello World!");
            helloWorldLabel.setTextColor(Color.GREEN);
            buttonSwitchBoolean = true;
        }
        else
        {
            helloWorldLabel.setText("Hello CS175!");
            helloWorldLabel.setTextColor(Color.BLACK);

            buttonSwitchBoolean = false;
        }
    }
}