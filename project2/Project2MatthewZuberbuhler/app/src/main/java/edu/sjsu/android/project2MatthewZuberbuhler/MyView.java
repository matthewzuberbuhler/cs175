package edu.sjsu.android.project2MatthewZuberbuhler;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManager;
import android.view.Display;
import android.view.View;

public class MyView extends View implements SensorEventListener {

    // You can change the ball size if you want
    private static final int BALL_SIZE = 100;
    private Bitmap field;
    private Bitmap ball;
    private float XOrigin, YOrigin, horizontalBound, verticalBound;
    private final Particle mBall = new Particle();

    // Paint object is used to draw your name
    private Paint paint = new Paint();

    // HINT: 2 of them are classes in the sensor framework
    private SensorManager manager;
    private Sensor acc;
    //      1 is used for getting the rotation from "natural" orientation
    private Display display;
    //      4 of them are used for the sensor data (3 axes + timestamp)
    private Float x = 0f;
    private Float y = 0f;
    private Float z = 0f;
    private long timestamp = 0l;

    private float rotationOffset = 0f;



    public MyView(Context context) {
        super(context);

        // You will see errors here because there are no image files yet.
        // Add the images under drawable folder to get rid of the errors.
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        ball = Bitmap.createScaledBitmap(b, BALL_SIZE, BALL_SIZE, true);
        field = BitmapFactory.decodeResource(getResources(), R.drawable.field);

        display = ((DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE)).getDisplays()[0];

        manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
        {
            acc = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }

        XOrigin = Resources.getSystem().getDisplayMetrics().widthPixels / 2;
        YOrigin = Resources.getSystem().getDisplayMetrics().heightPixels / 2;
        horizontalBound = (Resources.getSystem().getDisplayMetrics().widthPixels - BALL_SIZE) / 2;
        verticalBound = (Resources.getSystem().getDisplayMetrics().heightPixels - BALL_SIZE) / 2;

        timestamp = System.nanoTime();

        field = Bitmap.createScaledBitmap(field, Resources.getSystem().getDisplayMetrics().widthPixels, Resources.getSystem().getDisplayMetrics().heightPixels , true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the field and the ball

        canvas.drawBitmap(field, 0, 0, null);
        canvas.drawBitmap(ball,
                (XOrigin - BALL_SIZE / 2f) + mBall.mPosX,
                (YOrigin - BALL_SIZE / 2f) - mBall.mPosY, null);




        paint.setTextSize(100);
        canvas.drawText(getResources().getString(R.string.Matthew), 0, YOrigin, paint);

        mBall.updatePosition(x,y,z,timestamp);
        mBall.resolveCollisionWithBounds(horizontalBound,verticalBound);
        invalidate();
    }

    public void startSimulation() {
        manager.registerListener(this, acc, SensorManager.SENSOR_DELAY_UI);
    }

    public void stopSimulation() {
        manager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // Remember to interpret the data as discussed in Lesson 8 page 16.
        x = sensorEvent.values[0];
        y = sensorEvent.values[1];
        z = sensorEvent.values[2];
        timestamp = System.nanoTime();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}