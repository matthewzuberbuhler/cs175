package com.example.project1matthewzuberbuhler;

import static org.junit.Assert.*;

import org.junit.Test;

public class MortageCalculatorUtilTest {
    @Test
    public void calculateMortage1() {
        Double value = MortageCalculatorUtil.calculateMortage(
                10000.0,
                5.5,
                15,
                false);
        assertEquals(value, 81.71, 0.001);
    }

    @Test
    public void calculateMortage2() {
        Double value = MortageCalculatorUtil.calculateMortage(
                20000.0,
                0.0,
                20,
                false);
        assertEquals(83.33, value, 0.001);
    }

    @Test
    public void calculateMortage3() {
        Double value = MortageCalculatorUtil.calculateMortage(
                10000.0,
                5.5,
                15,
                true);
        assertEquals(91.71, 91.71, 0.001);
    }

    @Test
    public void calculateMortage4() {
        Double value = MortageCalculatorUtil.calculateMortage(
                20000.0,
                10.0,
                20,
                true);
        assertEquals(213.00, value, 0.001);
    }

    @Test
    public void calculateMortage5() {
        Double value = MortageCalculatorUtil.calculateMortage(
                10000.0,
                10.0,
                30,
                true);
        assertEquals(97.76, value, 0.001);
    }
}

///Users/zuberbuhler/AndroidStudioProjects/cs175/project1/Project1MatthewZuberbuhler/app/src/androidTest/java/com/example/project1matthewzuberbuhler/ExampleInstrumentedTest.java