package com.example.project1matthewzuberbuhler;

public class MortageCalculatorUtil {
    public static Double calculateMortage(Double p, Double interestRate, int loanTerm, boolean includeTaxes)
    {
        Double n = loanTerm * 12.0;
        Double t = 0.001 * p;
        if(interestRate < 0.1)
        {
            if(includeTaxes)
            {
                return (double)Math.round((p / n + t) * 100d) / 100d;
            }
            else
            {
                return (double)Math.round((p / n) * 100d) / 100d;
            }
        }
        else
        {
            Double j = interestRate / 100 / 12.0;
            Double denom = 1.0 - Math.pow(1.0 + j, -n);

            if(includeTaxes)
            {
                return (double)Math.round((p * j / denom + t) * 100d) / 100d;
            }
            else
            {
                return (double)Math.round((p * j / denom) * 100d) / 100d;
            }
        }
    }
}
