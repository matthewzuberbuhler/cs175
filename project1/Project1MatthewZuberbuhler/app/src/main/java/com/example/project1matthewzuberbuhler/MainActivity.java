package com.example.project1matthewzuberbuhler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.project1matthewzuberbuhler.databinding.ActivityMainBinding;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private Double interestRate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());

        View view = binding.getRoot();
        setContentView(view);

        this.interestRate = 0.0;

        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                float currentProgress = progress * 0.1f;

                String yourProgress = String.format("%.1f", currentProgress);

                interestRate = Double.parseDouble(yourProgress);

                binding.interestPercentageLabel.setText( yourProgress + "%");// + seekBar.getMax());
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO this method declaration is required
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO this method declaration is required
            }
        });

        binding.calculateButton.setOnClickListener(this::onClickCalculateButton);

        binding.uninstallButton.setOnClickListener(this::onClickUninstallButton);
    }

    public void onClickCalculateButton(View view) {
        if (binding.principleTextfield .getText().length() == 0) {

            Toast.makeText(this, "Invalid Input!", Toast.LENGTH_LONG).show();

            binding.footerMessageLabel.setText(getResources().getString(R.string.enterPrincipleMessage));

            return;
        }
        else
        {


            try {
                double inputValue = Double.parseDouble(binding.principleTextfield.getText().toString());

                boolean hasMoreThanTwoDecimalPlaces = (BigDecimal.valueOf(inputValue).scale() > 2);

                if(hasMoreThanTwoDecimalPlaces)
                {
                    binding.footerMessageLabel.setText(getResources().getString(R.string.decimalPlacesMessage));

                    Toast.makeText(MainActivity.this, "Invalid Input!", Toast.LENGTH_SHORT).show();

                    return;
                }
                else
                {
                    int selectedId = binding.radioGroup.getCheckedRadioButtonId();

                    RadioButton selectedRadioButton = findViewById(selectedId);

                    int idx = binding.radioGroup.indexOfChild(selectedRadioButton);

                    int loanTerms[] = {15, 20, 30};

                    double mortgage = MortageCalculatorUtil.calculateMortage(
                            inputValue,
                            interestRate,
                            loanTerms[idx],
                            binding.includeTaxesCheckbox.isChecked()
                    );

                    String footerText = getResources().getString(R.string.monthPaymentMessage) + String.format("%.2f", mortgage);

                    binding.footerMessageLabel.setText(footerText);

                    return;
                }
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Invalid Input!", Toast.LENGTH_LONG).show();

                binding.footerMessageLabel.setText(getResources().getString(R.string.enterValidPrincipleMessage));

                return;
            }
        }
    }

    public void onClickUninstallButton(View view)
    {
        Intent delete = new Intent(Intent.ACTION_DELETE,
                Uri.parse("package:" + getPackageName()));
        startActivity(delete);
    }
}